<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('about', function () {
    return view('about');
});

Route::get('projects', function () {
    return view('projects');
});

Route::get('projects/multifamiliar', function () {
    return view('project_multifamiliar');
});

Route::get('projects/lachacrita', function () {
    return view('project_lachacrita');
});

Route::get('projects-done-{id}', function ($id) {
    switch ($id) {
        case 1:
            return view('projectsdone_1');
            break;
        case 2:
            return view('projectsdone_2');
            break;
        case 3:
            return view('projectsdone_3');
            break;
        case 4:
            return view('projectsdone_4');
            break;
        case 5:
            return view('projectsdone_5');
            break;
        case 6:
            return view('projectsdone_6');
            break;
        case 7:
            return view('projectsdone_7');
            break;
        case 8:
            return view('projectsdone_8');
            break;
        case 9:
            return view('projectsdone_9');
            break;
        case 10:
            return view('projectsdone_10');
            break;
        case 11:
            return view('projectsdone_11');
            break;
        case 12:
            return view('projectsdone_12');
            break;
        default:
            abort(404);
    }
    
});

Route::get('business', function () {
    return view('business');
});

Route::get('oportunities', function () {
    return view('oportunities');
});

Route::get('news', function () {
    return view('news');
});

Route::get('contact', function () {
    return view('contact');
});