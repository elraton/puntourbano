var siema;

window.addEventListener('load', function(){
    
    ScrollReveal().reveal('.reveal', {
        delay: 0,
        interval: 80,
        scale: 0.85,
        duration: 1000,
        reset: true,
    })
    

    const glider = new Glider(document.querySelector('.glider-contain'), {
        slidesToShow: 1,
        slidesToScroll: 1,
        rewind: true,
        dots: '#dots',
        draggable: false
    });

    function sliderAuto(slider, miliseconds) {
        slider.isLastSlide = function() {
            return slider.page >= slider.dots.childElementCount - 1;
        }
        
        var slide = function() {
            slider.slideTimeout = setTimeout(function() {
                function slideTo() {
                    return slider.isLastSlide() ? 0 : slider.page + 1;
                }
                slider.scrollItem(slideTo(), true);
            }, miliseconds);
        }

        slider.ele.addEventListener('glider-animated', function(event) {
            window.clearInterval(slider.slideTimeout);
            slide();
        });
        
        slide();
    }

    sliderAuto(glider, 3000);

    if ( this.document.getElementById('plan-carousel') ) {
        siema = new Siema({
            selector: '#plan-carousel',
            duration: 200,
            easing: 'ease-out',
            perPage: 1,
            startIndex: 0,
            draggable: true,
            multipleDrag: true,
            threshold: 20,
            loop: false,
            rtl: false,
            onInit: () => {},
            onChange: () => {},
        });

        const arrows_left = document.getElementsByClassName('plans_left');
        for ( const obj of arrows_left ) {
            obj.onclick = () => {
                siema.prev();
            }
        }

        const arrow_right = document.getElementsByClassName('plans_right');
        for ( const obj of arrow_right ) {
            obj.onclick = () => {
                siema.next();
            }
        }
    }

    if ( this.document.getElementById('proj-gallery') ) {
        baguetteBox.run('#proj-gallery');
    }
});


document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems, {
        onOpenStart: () => {
            const plans = document.querySelectorAll('.plan-item');
            let i = siema.currentSlide;
            if ( (i+1) < plans.length ) {
                plans[i+1].style.opacity = 0;
            }
            if ( (i-1) >= 0 ) {
                plans[i-1].style.opacity = 0;
            }
        },
        onCloseEnd: () => {
            const plans = document.querySelectorAll('.plan-item');
            let i = siema.currentSlide;
            if ( (i+1) < plans.length ) {
                plans[i+1].style.opacity = 1;
            }
            if ( (i-1) >= 0 ) {
                plans[i-1].style.opacity = 1;
            }
        }
    });
});