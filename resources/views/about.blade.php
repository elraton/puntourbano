@extends('base')

@section('title', 'Quienes Somos')

@section('banner')
    <div>
        <img src="/images/2cabecera.jpg" alt="slider1" draggable="false">
    </div>
@endsection

@section('content')
    <div class="row about">

        <div class="col s12 about-content">

            <div class="about-item">
                <div class="row">
                    <div class="col s4 about-text">
                        <h3 class="title">Quiénes Somos</h3>
                        <p class="description">Punto urbano E.I.R.L es una empresa dedicada a la construcción y venta de proyectos inmobiliarios cuyas actividades se iniciaron en el año 2010 y continuaran hasta la fecha.
                                Siempre a la vanguardia está convencida que la constante innovación en sus propuestas son la base para satisfacer todas las necesidades del cliente; hasta el
                                momento ha sido creadora de diversos proyectos en Cerro Colorado,
                                Vallecito y Cayma, todos ellos con porcentajes de capacidad de pre-venta altísimos debido a la calidad de infraestructura y acabados que continuamente presenta. No
                                solo ansiamos generar valor, ansiamos generar un espacio innovador, de calidad y seguridad.</p>
                    </div>
                    <div class="col s8 about-image about-image-right">
                        <img class="image reveal" src="/images/2qs1.jpg" alt="qs1" draggable="false">
                    </div>
                </div>
            </div>

            <div class="spacer"></div>

            <div class="about-item">
                <div class="row">
                    <div class="col s8 about-image about-image-left">
                        <img class="image reveal" src="/images/2qs2.jpg" alt="qs2" draggable="false">
                    </div>
                    <div class="col s4 about-text">
                        <h3 class="title">Misión</h3>
                        <p class="description">Ofrecer un proyecto moderno, funcional y sostenible, que no sólo represente para nuestros clientes una propuesta de inversión inteligente, sino que les brinde un ambiente cálido y confortable donde puedan realizar sus actividades diarias.</p>
                        <h3 class="title">Visión</h3>
                        <p class="description">Nuestra meta es la de posicionarnos como una de las mejores constructoras de la ciudad, conformada por un equipo multidisciplinario y competitivo, cuyas habilidades nos permitan ganar la confianza de más inversores, demostrándoles que podemos alcanzar nuevos niveles de desarrollo en base a un continuo proceso de innovación e ingeniería.</p>
                    </div>
                </div>
            </div>
            
            <div class="flame">
                <img src="/images/flame.svg" alt="flames">
            </div>

        </div>
    </div>
@endsection

@section('footer')
<div class="container">
    <div class="row">
        <div class="col s10 footer-slogan">
            <img src="/images/footerslogan.svg" alt="footerslogan" draggable="false">
            <p class="description">Somos una empresa promotora y desarrolladora con más de 9 años de experiencia conjunta en el sector inmobiliario.</p>
        </div>
    </div>
</div>
@endsection