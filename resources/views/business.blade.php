@extends('base')

@section('title', 'Lineas de Negocio')

@section('banner')
    <div>
        <img src="/images/6cabecera.jpg" alt="slider1" draggable="false">
    </div>
@endsection

@section('content')
    <div class="row about">

        <div class="col s12 about-content" style="margin-bottom: 0;">

            <div class="about-item">
                <div class="row">
                    <div class="col s4 about-text">
                        <h3 class="title">Lineas de Negocio</h3>
                        <p class="description">Punto urbano E.I.R.L es una empresa dedicada a la construcción y venta de proyectos inmobiliarios cuyas actividades se iniciaron en el año 2010 y continuaran hasta la fecha. 
                            Siempre a la vanguardia está convencida que la constante innovación en sus propuestas son la base para satisfacer todas las necesidades del cliente; hasta el
                                momento ha sido creadora de diversos proyectos en Cerro Colorado, Vallecito y Cayma, todos ellos con porcentajes de
                                capacidad de pre-venta altísimos debido a la calidad de
                                infraestructura y acabados que continuamente presenta. No
                                solo ansiamos generar valor, ansiamos generar un espacio
                                innovador, de calidad y seguridad</p>
                    </div>
                    <div class="col s8 about-image about-image-right">
                        <img class="image reveal" src="/images/2qs1.jpg" alt="qs1" draggable="false">
                    </div>
                </div>
            </div>

            <div class="businesstitle">
                <h2>Asesoría Especializada</h2>
            </div>

            <div class="businessdescription">
                <div class="row">
                    <div class="col s7 text">
                        <h5 class="title">Análisis Terreno Ubicación</h5> 
                        <p class="description">Ofrecer un proyecto moderno, funcional y sostenible, que no sólo represente para nuestros clientes una propuesta de inversión inteligente, sino que les brinde un ambiente cálido y confortable donde puedan realizar sus actividades diarias.</p>
                    </div>
                    <div class="col s5 image">
                        <img class="reveal" src="/images/business.jpg" alt="business" draggable="false">
                    </div>
                </div>
            </div>

            <div class="businesstitle">
                <h2>Gestión de Proyectos</h2>
            </div>

            <div class="businessdescription">
                <div class="row">
                    <div class="col s7 text">
                        <h5 class="title">Planos</h5> 
                        <p class="description">Texto Planos Texto Planos Texto Planos Texto Planos Texto Planos Texto Planos Texto Planos Texto
                                Planos
                                Texto Planos
                                Texto Planos
                                Texto Planos
                                Texto Planos
                                Texto Planos
                                Texto Planos
                                Texto Planos </p>
                    </div>
                    <div class="col s5 image">
                        <img class="reveal" src="/images/business.jpg" alt="business" draggable="false">
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('footer')
<div class="container">
    <div class="row">
        <div class="col s10 footer-slogan">
            <img src="/images/footerslogan.svg" alt="footerslogan" draggable="false">
            <p class="description">Somos una empresa promotora y desarrolladora con más de 9 años de experiencia conjunta en el sector inmobiliario.</p>
        </div>
    </div>
</div>
@endsection