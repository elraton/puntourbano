@extends('base')

@section('title', 'Contacto')

@section('banner')
    <div>
        <img src="/images/9contactcabecera.jpg" alt="slider1" draggable="false">
    </div>
@endsection

@section('content')
    <div class="row contact">

        <div class="col s12 slogan">
            
                <p>Si tienes alguna duda o inquietud sobre</p>
                <p>nuestros proyectos</p>
                <p>no dudes en comunicarte con nosotros.</p>
                <p>Completa el siguiente formulario,</p>
                <p>nos contactaremos contigo lo más pronto posible</p>

        </div>
    </div>
@endsection

@section('footer')
<div class="container">
    <div class="row">
        <form action="#" class="col s8 form-contact">
            <p>¿Te interesa conocer más sobre nuestros proyectos?</p>
            <p>Haz tu cita y comienza una nueva vida.</p>
            <div class="spacer"></div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="name" type="text" class="validate">
                    <label for="name">Nombre</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input id="email" type="email" class="validate">
                    <label for="email">Correo Electrónico</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <input id="phone" type="text" class="validate">
                    <label for="phone">Teléfono</label>
                </div>
                <div class="input-field col s6">
                    <input id="city" type="text" class="validate">
                    <label for="city">Ciudad</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <textarea id="message" class="materialize-textarea"></textarea>
                    <label for="message">Mensaje</label>
                </div>
            </div>
            
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn btn-urbano">ENVIAR</button>
                </div>
            </div>
        </form>
        <div class="col s4">
            <h2 class="title">Contacto</h2>
        </div>
    </div>
</div>
@endsection