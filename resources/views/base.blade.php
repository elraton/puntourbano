<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/glider.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.0/baguetteBox.min.css">
    <link href="/css/responsive.css?v1.0" rel="stylesheet">
    <link href="/css/style.css?v1.0" rel="stylesheet">
</head>
<body>
    <div class="container-fluid navbar">
        @include('navbar')
    </div>
    <div class="container-fluid slider">
        <div class="row">
            <div class="col s12 banner">
                @yield('banner')
            </div>
        </div>
        <div class="row">
            <div class="col s12 glider-contain">
                @yield('slider')
            </div>
            <div id="dots"></div>
        </div>
    </div>
    <div class="container">
        @yield('content')
    </div>
    <div class="container-fluid footer">
        @yield('footer')
    </div>
    <div class="container-fluid footer-info">
        <div class="container">
            <div class="row">
                <div class="col s6 footer-logo">
                    <a href="{{ url('/') }}">
                        <img src="/images/logo.svg" alt="logo">
                    </a>
                </div>
                <div class="col s6 footer-info">
                    <div class="social">
                        <a class="social-icon" href="#">
                            <img src="/images/icons/facebook-icon.svg" alt="facebook">
                        </a>
                        <a class="social-icon" href="#">
                            <img src="/images/icons/instagram-icon.svg" alt="instagram">
                        </a>
                        <a class="social-icon" href="#">
                            <img src="/images/icons/youtube-icon.svg" alt="youtube">
                        </a>
                    </div>
                    <p class="footer-info-desc">Av. Ejército 710 - Of. 905 - Yanahuara</p>
                    <p class="footer-info-desc">Teléfono: 054 - 213871 - Cels: 977 136 945 - 993 329 626</p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="/js/glider.js"></script>
    <script src="/js/siema.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.0/baguetteBox.js"></script>
    <script src="/js/scrollreveal.min.js"></script>
    <script src="/js/app.js"></script>
    
</body>
</html>