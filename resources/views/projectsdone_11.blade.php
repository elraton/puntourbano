@extends('base')

@section('title', 'Proyectos concluidos')

@section('banner')
    <div>
        <img src="/images/5cabecera.jpg" alt="slider1" draggable="false">
    </div>
@endsection

@section('content')
    <div class="row project-detail">

        <div class="col s12 projects-construction">
        
            <div class="project-item">
                <div class="row">
                    <div class="col s6 project-text">
                        <h3 class="title">Edificio</h3>
                        <h4 class="subtitle">Torre Uno</h4>
                        <div class="spacer"></div>
                        <p class="description">Edificio Multifamiliar "Torre Uno" . 6 niveles, Zona Residencial Urb. Los Pinos, Vallecito, vista al parque.</p>
                    </div>
                    <div class="col s6 project-image">
                        <img class="image reveal" src="/images/projects/1proyecto1.jpg" alt="project1" draggable="false">
                    </div>
                </div>
            </div>

        </div>

        <div class="col s12 project-information">
            <div class="info-item">
                <div class="row">
                    <div class="col s6 info-text">
                        <h3 class="title">Ubicación</h3>
                        <p class="description">Zona Residencial Urb. Los Pinos, Vallecito.</p> 
                    </div>
                    <div class="col s6 info-image">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1112.2201322064789!2d-71.54744503133955!3d-16.388707508171546!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91424a421176400b%3A0x6baba468cc6d758c!2sScotiabank!5e0!3m2!1ses!2spe!4v1565149904963!5m2!1ses!2spe" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 project-gallery" style="border-top: none;">
            <h2 class="title">Galería</h2>
            <div class="row" id="proj-gallery">
                <div class="col s9">
                    <div class="row">
                        <div class="col s4">
                            <a href="/images/5p1pasGal1grande.jpg" style="background: url('/images/5pro1pasadoGal1.jpg')">
                            </a>
                        </div>
                        <div class="col s4">
                            <a href="/images/5p1pasGal2grande.jpg" style="background: url('/images/4pro1Gal2.jpg')">
                            </a>
                        </div>
                        <div class="col s4">
                            <a href="/images/5p1pasGal3grande.jpg" style="background: url('/images/4pro1Gal3.jpg')">
                            </a>
                        </div>
                        <div class="col s4">
                            <a href="/images/5p1pasGal4grande.jpg" style="background: url('/images/4pro1Gal4.jpg'); margin-top: 1rem;">
                            </a>
                        </div>
                        <div class="col s8">
                            <a href="/images/5p1pasGal5grande.jpg" style="background: url('/images/4pro1Gal5.jpg'); margin-top: 1rem;">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col s3">
                    <a href="/images/5p1pasGal6grande.jpg" style="background: url('/images/4pro1Gal6.jpg')">
                    </a>
                </div>
            </div>
        </div>

        <div class="col s12 share">
            <span>Compartir</span>
            <a href="">
                <img src="/images/facebook.svg" alt="facebook">
            </a>
            <a href="">
                <img src="/images/whatsapp.svg" alt="facebook">
            </a>
        </div>


    </div>
@endsection

@section('footer')
<div class="container">
    <div class="row">
        <div class="col s10 footer-slogan">
            <img src="/images/footerslogan.svg" alt="footerslogan" draggable="false">
            <p class="description">Somos una empresa promotora y desarrolladora con más de 9 años de experiencia conjunta en el sector inmobiliario.</p>
        </div>
    </div>
</div>
@endsection