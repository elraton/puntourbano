@extends('base')

@section('title', 'Home')

@section('slider')
    <div>
        <img src="/images/1SLIDERhome1.jpg" alt="slider1" draggable="false">
        <div class="moreinfo">
            <img class="image" src="/images/logo.svg" alt="logobanner">
            <h2 class="title1">Tu nuevo comienzo empieza hoy</h2>
        </div>
    </div>
    <div>
        <img src="/images/1SLIDERhome2.jpg" alt="slider2" draggable="false">
        <div class="moreinfo">
            <div class="poster">
                <h2 class="title">Pre Venta</h2>
                <span class="subtitle">DEPARTAMENTOS</span>
            </div>
            <h2 class="title2">Edificio</h2>
            <h2 class="title1">La Chacrita - Cayma</h2>
        </div>
    </div>
    <div>
        <img src="/images/1SLIDERhome3.jpg" alt="slider3" draggable="false">
        <div class="moreinfo">
            <div class="poster">
                <h2 class="title">DEPARTAMENTOS</h2>
                <span class="subtitle">EN CONSTRUCCION</span>
            </div>
            <h2 class="title2">Multifamiliar</h2>
            <h2 class="title1">Ingenieros - Cayma</h2>
        </div>
    </div>
    <div>
        <img src="/images/1SLIDERhome4.jpg" alt="slider4" draggable="false">
        <div class="moreinfo">
            <h2 class="title1">El lugar perfecto para tu familia</h2>
        </div>
    </div>
@endsection

@section('content')
    <div class="row home">
        <div class="col s12">

            <div class="row">
                <div class="col s4 home-icons">
                    <img class="icon reveal" src="/images/proyects.svg" alt="proyects" draggable="false">
                    <h2 class="title">11 Proyectos</h2>
                    <h5 class="subtitle">165 DEPARTAMENTOS</h5>
                </div>
                <div class="col s4 home-icons">
                    <img class="icon reveal" src="/images/building.svg" alt="proyects" draggable="false">
                    <h2 class="title">109,500 m2</h2>
                    <h5 class="subtitle">CONSTRUIDOS</h5>
                </div>
                <div class="col s4 home-icons">
                    <img class="icon reveal" src="/images/persons.svg" alt="proyects" draggable="false">
                    <h2 class="title">MÁS DE 200</h2>
                    <h5 class="subtitle">COLABORADORES</h5>
                </div>
            </div>

        </div>

        <div class="col s12 slogan">
            <h2 class="text">Compartimos con nuestros clientes la ilusión de su proyecto que intentamos plasmar en la obra bien ejecutada.</h2>
        </div>

        <div class="col s12 projects">
            <h2 class="title">Proyectos en desarrollo</h2>
        </div>

        <div class="col s12 projects-content">

            <div class="project-item">
                <div class="row">
                    <div class="col s6 project-text">
                        <h3 class="title">Multifamiliar</h3>
                        <h4 class="subtitle">Ingenieros</h4>
                        <div class="spacer"></div>
                        <p class="description">El edificio Multifamiliar contará con 14 departamentos, con dos niveles de cocheras ubicados en el sótano 1 y sótano 2, con capacidad para 16 estacionameintos.</p>
                        <div class="button-cont">
                            <a class="button" href="{{ url('projects/multifamiliar')}}">Ver más</a>
                        </div>
                    </div>
                    <div class="col s6 project-image">
                        <img class="image reveal" src="/images/projects/1proyecto1.jpg" alt="project1" draggable="false">
                    </div>
                </div>
            </div>

            <div class="project-item">
                <div class="row">
                    <div class="col s6 project-text">
                        <h3 class="title">Edificio</h3>
                        <h4 class="subtitle">La Chacrita - Cayma</h4>
                        <div class="spacer"></div>
                        <p class="description">Edificio Multifamiliar contará con 10 departamentos de 3 y 4 dormitorios. Edificio Multifamiliar contará con 10 departamentos de 3 y 4 dormitorios. Edificio Multifamiliar contará con 10 departamentos de 3 y 4 dormitorios.</p>
                        <div class="button-cont">
                            <a class="button" href="{{ url('projects/lachacrita')}}">Ver más</a>
                        </div>
                    </div>
                    <div class="col s6 project-image">
                        <img class="image reveal" src="/images/projects/1proyecto2.jpg" alt="project1" draggable="false">
                    </div>
                </div>
            </div>

            <div class="flame">
                <img src="/images/flame.svg" alt="flames">
            </div>

        </div>
    </div>
@endsection

@section('footer')
<div class="container">
    <div class="row">
        <form action="#" class="col s8 form-contact">
            <p>¿Te interesa conocer más sobre nuestros proyectos?</p>
            <p>Haz tu cita y comienza una nueva vida.</p>
            <div class="spacer"></div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="name" type="text" class="validate">
                    <label for="name">Nombre</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input id="email" type="email" class="validate">
                    <label for="email">Correo Electrónico</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <input id="phone" type="text" class="validate">
                    <label for="phone">Teléfono</label>
                </div>
                <div class="input-field col s6">
                    <input id="city" type="text" class="validate">
                    <label for="city">Ciudad</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <textarea id="message" class="materialize-textarea"></textarea>
                    <label for="message">Mensaje</label>
                </div>
            </div>
            
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn btn-urbano">ENVIAR</button>
                </div>
            </div>
        </form>
        <div class="col s4">
            <h2 class="title">Contacto</h2>
        </div>
    </div>
</div>
@endsection