@extends('base')

@section('title', 'Noticias')

@section('banner')
    <div>
        <img src="/images/8cabecera.jpg" alt="slider1" draggable="false">
    </div>
@endsection

@section('content')
    <div class="row news">

        <div class="col s12">

            <div class="news-item">
                <div class="row">
                    <div class="col s12 news-text">
                        <h3 class="title">Conoce los distritos que liderarán oferta inmobiliaria en Lima</h3>
                        <p class="description">Debido a una serie de condiciones favorables y normativas municipales, los distritos de La Victoria, Breña y Lima Cercado se encaminan a
                                liderar en el mediano plazo la oferta inmobiliaria de la capital.</p>
                        <p class="description">"En 10 años, Santa Catalina ubicada en el distrito de La Victoria, será un mix entre Lince y San Isidro. Tiene gran potencial para la demanda de
                                vivienda con un valor promedio actual por metro cuadrado de 1.500 a 1.600 dólares”, explicó Jaime Paredes, gerente general de Urbana Perú.
                                Hoy en día, la vivienda representa la mayor oferta en la zona; sin embargo, se espera la llegada de oficinas boutique y la continuidad de los
                                servicios y apoyo por parte del municipio.</p>
                        <span class="subtitle">Distritos tradicionales</span>
                        <p class="description">La zona industrial de Cercado de Lima y Breña son otras de las zonas con proyección en la capital. Las avenidas Venezuela, Colonial y Argentina
                                serán foco de muchas inmobiliarias en poco tiempo. También se encuentra Ate, por la cercanía a la línea 2 del Metro, es decir, la Carretera
                                Central.</p>
                        <p class="description">En el caso particular de Cercado y Breña, podrían encontrarse propiedades alrededor de 1.100 dólares el metro cuadrado que pasarán en corto
                                tiempo a valorizarse en más de 1.400 dólares por metro cuadrado.</p>
                        <p class="description">"Todo ello se consolidará en la medida que existan las condiciones para un cambio de zonificación asociado al desarrollo de servicios básicos
                                que permitan un crecimiento inmobiliario ordenado. Sólo el trabajo conjunto hará que éstos distritos, luzcan un nuevo rostro", acotó Paredes.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('footer')
<div class="container">
    <div class="row">
        <div class="col s10 footer-slogan">
            <img src="/images/footerslogan.svg" alt="footerslogan" draggable="false">
            <p class="description">Somos una empresa promotora y desarrolladora con más de 9 años de experiencia conjunta en el sector inmobiliario.</p>
        </div>
    </div>
</div>
@endsection