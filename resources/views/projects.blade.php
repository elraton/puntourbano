@extends('base')

@section('title', 'Proyectos')

@section('banner')
    <div>
        <img src="/images/3cabecera.jpg" alt="slider1" draggable="false">
    </div>
@endsection

@section('content')
    <div class="row projects">
        <div class="col s12 first-title">
            <h1 class="title">Una buena ubicación siempre nos inspira</h1>
            <p class="description">Compartimos con nuestros clientes la ilusión de su proyecto que intentamos plasmar en la obra bien ejecutada.</p>
            <p class="description">Las ideas y los proyectos que nos ilusionan nos mueven a crear, diseñar, construir y materializar espacios donde compartir experiencias.</p>
        </div>

        <div class="col s12 projects-title">
            <h2 class="title">Proyectos en desarrollo</h2>
        </div>

        <div class="col s12 projects-construction">
        
            <div class="project-item">
                <div class="row">
                    <div class="col s6 project-text">
                        <h3 class="title">Multifamiliar</h3>
                        <h4 class="subtitle">Ingenieros</h4>
                        <div class="spacer"></div>
                        <p class="description">El edificio Multifamiliar contará con 14 departamentos, con dos niveles de cocheras ubicados en el sótano 1 y sótano 2, con capacidad para 16 estacionameintos.</p>
                        <div class="button-cont">
                            <a class="button" href="{{ url('projects/multifamiliar')}}">Ver más</a>
                        </div>
                    </div>
                    <div class="col s6 project-image">
                        <img class="image reveal" src="/images/projects/1proyecto1.jpg" alt="project1" draggable="false">
                    </div>
                </div>
            </div>
            <div class="project-item">
                <div class="row">
                    <div class="col s6 project-text">
                        <h3 class="title">Edificio</h3>
                        <h4 class="subtitle">La Chacrita - Cayma</h4>
                        <div class="spacer"></div>
                        <p class="description">Edificio Multifamiliar contará con 10 departamentos de 3 y 4 dormitorios. Edificio Multifamiliar contará con 10 departamentos de 3 y 4 dormitorios. Edificio Multifamiliar contará con 10 departamentos de 3 y 4 dormitorios.</p>
                        <div class="button-cont">
                            <a class="button" href="{{ url('projects/lachacrita')}}">Ver más</a>
                        </div>
                    </div>
                    <div class="col s6 project-image">
                        <img class="image reveal" src="/images/projects/1proyecto2.jpg" alt="project1" draggable="false">
                    </div>
                </div>
            </div>

        </div>

        <div class="col s12 projects-title2">
            <h2 class="title">Proyectos concluidos</h2>

            <p class="description">Punto urbano E.I.R.L es una empresa dedicada a la construcción y venta de proyectos inmobiliarios cuyas actividades se
iniciaron en el año 2010 y continuaran hasta la fecha. Siempre a la vanguardia está convencida que la constante innovación en sus
propuestas son la base para satisfacer todas las necesidades del cliente; hasta el momento ha sido creadora de diversos
proyectos en Cerro Colorado, Vallecito y Cayma, todos ellos con porcentajes de capacidad de pre-venta altísimos debido a la
calidad de infraestructura y acabados que continuamente presenta. No solo ansiamos generar valor, ansiamos generar un
espacio innovador, de calidad y seguridad.spacios interiores de los departamentos.</p>
        </div>

        

        <div class="col s12 projects-done">
            <div class="row">
                <a href="{{ url('projects-done-1') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado1.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-2') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado2.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-3') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado3.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-4') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado4.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-5') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado5.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-6') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado6.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-7') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado7.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-8') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado8.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-9') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado9.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-10') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado10.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-11') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado11.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>

                <a href="{{ url('projects-done-12') }}" class="col s4 project-item">
                    <img class="reveal" src="/images/3ppasado12.jpg" alt="edificio torre uno">
                    <span>EDIFICIO TORRE UNO</span>
                </a>
            </div>
        </div>

    </div>
@endsection