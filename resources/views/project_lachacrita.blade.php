@extends('base')

@section('title', 'La Chacrita')

@section('banner')
    <div>
        <img src="/images/4cabecera.jpg" alt="slider1" draggable="false">
    </div>
@endsection

@section('content')
    <div class="row project-detail">

        <div class="col s12 projects-construction">
        
            <div class="project-item">
                <div class="row">
                    <div class="col s6 project-text">
                        <h3 class="title">La Chacrita</h3>
                        <h4 class="subtitle">Cayma</h4>
                        <div class="spacer"></div>
                        <p class="description">El edificio Multifamiliar contará con 14 departamentos, con dos niveles de cocheras ubicados en el sótano 1 y sótano 2, con capacidad para 16 estacionameintos.</p>
                    </div>
                    <div class="col s6 project-image">
                        <img class="image reveal" src="/images/projects/1proyecto1.jpg" alt="project1" draggable="false">
                    </div>
                </div>
            </div>

        </div>

        <div class="col s12 project-information">
            <div class="info-item">
                <div class="row">
                    <div class="col s6 info-text">
                        <h3 class="title">Ubicación</h3>
                        <p class="description">Se encuentra ubicado en Coop. Colegio de
                                Ingenieros Mza. A Lte. 19 - Cayma cuya
                                ubicación en esquina favorece al proyecto
                                teniendo dos frentes; aportando iluminación y
                                ventilación de los espacios interiores de los
                                departamentos.</p> 
                    </div>
                    <div class="col s6 info-image">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1112.2201322064789!2d-71.54744503133955!3d-16.388707508171546!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91424a421176400b%3A0x6baba468cc6d758c!2sScotiabank!5e0!3m2!1ses!2spe!4v1565149904963!5m2!1ses!2spe" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="info-item">
                <div class="row">
                    <div class="col s6 info-text">
                        <h3 class="title">Amenities</h3>
                        <ul class="list">
                            <li>DEPARTAMENTOS DE 3 DORMITORIOS</li>
                            <li>DORMITORIO PRINCIPAL CON BAÑO</li>
                            <li>SALA</li>
                            <li>COMEDOR</li>
                            <li>COCINA</li>
                            <li>LAVANDERIA</li>
                            <li>ESTACIONAMIENTO</li>
                            <li>CLOSETS</li>
                            <li>REPOSTEROS ALTOS Y BAJOS</li>
                            <li>INSTALACIÓN PARA GAS NATURAL</li>
                        </ul>
                    </div>
                    <div class="col s6 info-image">
                        <img class="image reveal" src="/images/projects/1proyecto1.jpg" alt="project1" draggable="false">
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 project-plans">
            <div id="plan-carousel">
                <div class="plan-item">
                    <div class="row">
                        <div class="col s5"></div>
                        <div class="col s7">
                            <h1 class="title">Dpto. Tipología A</h1>
                            <h2 class="subtitle">Area Total: m2</h2>
                        </div>
                        <div class="col s5 image-cont">
                            <img class="materialboxed image reveal" src="/images/4p1ta-grande.jpg" alt="project1" draggable="false">
                            {{-- <img class="image" src="/images/4p1ta.jpg" alt="project1" draggable="false"> --}}
                        </div>
                        <div class="col s7 plan-info">
                            <ul class="items">
                                <li>Sala</li>
                                <li>Cocina</li>
                                <li>Lavandería</li>
                                <li>Dormitorio principal con baño</li>
                                <li>3 dormitorios</li>
                                <li>Estacionamiento</li>
                                <li>Instalación para gas natural</li>
                            </ul>
                            <h5 class="title">Pisos</h5>
                            <div class="row">
                                <div class="col s5">Sala-Comedor</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Dormitorios</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Baños</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Cocina</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Lavandería</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>

                            <h5 class="title">Closet</h5>
                            <p class="description">Melamine blanca o color</p>

                            <h5 class="title">Reposteros</h5>
                            <p class="description">Altos y bajos de melamina, blancos o de color, con mesada de granito</p>

                            <div class="row">
                                <div class="col s12 buttons-cont">
                                    <button class="btn plans_left"><i class="material-icons">chevron_left</i></button>
                                    <button class="btn plans_right"><i class="material-icons">chevron_right</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="plan-item">
                    <div class="row">
                        <div class="col s5"></div>
                        <div class="col s7">
                            <h1 class="title">Dpto. Tipología B</h1>
                            <h2 class="subtitle">Area Total: m2</h2>
                        </div>
                        <div class="col s5 image-cont">
                            <img class="materialboxed image reveal" src="/images/4p1tb-grande.jpg" alt="project1" draggable="false">
                        </div>
                        <div class="col s7 plan-info">
                            <ul class="items">
                                <li>Sala</li>
                                <li>Cocina</li>
                                <li>Lavandería</li>
                                <li>Dormitorio principal con baño</li>
                                <li>3 dormitorios</li>
                                <li>Estacionamiento</li>
                                <li>Instalación para gas natural</li>
                            </ul>
                            <h5 class="title">Pisos</h5>
                            <div class="row">
                                <div class="col s5">Sala-Comedor</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Dormitorios</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Baños</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Cocina</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Lavandería</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>

                            <h5 class="title">Closet</h5>
                            <p class="description">Melamine blanca o color</p>

                            <h5 class="title">Reposteros</h5>
                            <p class="description">Altos y bajos de melamina, blancos o de color, con mesada de granito</p>

                            <div class="row">
                                <div class="col s12 buttons-cont">
                                    <button class="btn plans_left"><i class="material-icons">chevron_left</i></button>
                                    <button class="btn plans_right"><i class="material-icons">chevron_right</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="plan-item">
                    <div class="row">
                        <div class="col s5"></div>
                        <div class="col s7">
                            <h1 class="title">Dpto. Tipología C</h1>
                            <h2 class="subtitle">Area Total: m2</h2>
                        </div>
                        <div class="col s5 image-cont">
                            <img class="materialboxed image reveal" src="/images/4p1tc-grande.jpg" alt="project1" draggable="false">
                        </div>
                        <div class="col s7 plan-info">
                            <ul class="items">
                                <li>Sala</li>
                                <li>Cocina</li>
                                <li>Lavandería</li>
                                <li>Dormitorio principal con baño</li>
                                <li>3 dormitorios</li>
                                <li>Estacionamiento</li>
                                <li>Instalación para gas natural</li>
                            </ul>
                            <h5 class="title">Pisos</h5>
                            <div class="row">
                                <div class="col s5">Sala-Comedor</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Dormitorios</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Baños</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Cocina</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Lavandería</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>

                            <h5 class="title">Closet</h5>
                            <p class="description">Melamine blanca o color</p>

                            <h5 class="title">Reposteros</h5>
                            <p class="description">Altos y bajos de melamina, blancos o de color, con mesada de granito</p>
                            
                            <div class="row">
                                <div class="col s12 buttons-cont">
                                    <button class="btn plans_left"><i class="material-icons">chevron_left</i></button>
                                    <button class="btn plans_right"><i class="material-icons">chevron_right</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="plan-item">
                    <div class="row">
                        <div class="col s5"></div>
                        <div class="col s7">
                            <h1 class="title">Dpto. Tipología D</h1>
                            <h2 class="subtitle">Area Total: m2</h2>
                        </div>
                        <div class="col s5 image-cont">
                            <img class="materialboxed image reveal" src="/images/4p1td-grande.jpg" alt="project1" draggable="false">
                        </div>
                        <div class="col s7 plan-info">
                            <ul class="items">
                                <li>Sala</li>
                                <li>Cocina</li>
                                <li>Lavandería</li>
                                <li>Dormitorio principal con baño</li>
                                <li>3 dormitorios</li>
                                <li>Estacionamiento</li>
                                <li>Instalación para gas natural</li>
                            </ul>
                            <h5 class="title">Pisos</h5>
                            <div class="row">
                                <div class="col s5">Sala-Comedor</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Dormitorios</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Baños</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Cocina</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Lavandería</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>

                            <h5 class="title">Closet</h5>
                            <p class="description">Melamine blanca o color</p>

                            <h5 class="title">Reposteros</h5>
                            <p class="description">Altos y bajos de melamina, blancos o de color, con mesada de granito</p>

                            <div class="row">
                                <div class="col s12 buttons-cont">
                                    <button class="btn plans_left"><i class="material-icons">chevron_left</i></button>
                                    <button class="btn plans_right"><i class="material-icons">chevron_right</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="plan-item">
                    <div class="row">
                        <div class="col s5"></div>
                        <div class="col s7">
                            <h1 class="title">Dpto. Tipología EDUPLEX</h1>
                            <h2 class="subtitle">Area Total: m2</h2>
                        </div>
                        <div class="col s5 image-cont">
                            <img class="materialboxed image reveal" src="/images/4p1te-grande.jpg" alt="project1" draggable="false">
                        </div>
                        <div class="col s7 plan-info">
                            <ul class="items">
                                <li>Sala</li>
                                <li>Cocina</li>
                                <li>Lavandería</li>
                                <li>Dormitorio principal con baño</li>
                                <li>3 dormitorios</li>
                                <li>Estacionamiento</li>
                                <li>Instalación para gas natural</li>
                            </ul>
                            <h5 class="title">Pisos</h5>
                            <div class="row">
                                <div class="col s5">Sala-Comedor</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Dormitorios</div>
                                <div class="col s7">: Laminado</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Baños</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Cocina</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>
                            <div class="row">
                                <div class="col s5">Lavandería</div>
                                <div class="col s7">: Porcelanato</div>
                            </div>

                            <h5 class="title">Closet</h5>
                            <p class="description">Melamine blanca o color</p>

                            <h5 class="title">Reposteros</h5>
                            <p class="description">Altos y bajos de melamina, blancos o de color, con mesada de granito</p>

                            <div class="row">
                                <div class="col s12 buttons-cont">
                                    <button class="btn plans_left"><i class="material-icons">chevron_left</i></button>
                                    <button class="btn plans_right"><i class="material-icons">chevron_right</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col s12 project-gallery">
            <h2 class="title">Galería</h2>
            <div class="row" id="proj-gallery">
                <div class="col s9">
                    <div class="row">
                        <div class="col s4">
                            <a href="/images/4p1Gal1grande.jpg" style="background: url('/images/4pro1Gal1.jpg')">
                            </a>
                        </div>
                        <div class="col s4">
                            <a href="/images/4p1Gal2grande.jpg" style="background: url('/images/4pro1Gal2.jpg')">
                            </a>
                        </div>
                        <div class="col s4">
                            <a href="/images/4p1Gal3grande.jpg" style="background: url('/images/4pro1Gal3.jpg')">
                            </a>
                        </div>
                        <div class="col s4">
                            <a href="/images/4p1Gal4grande.jpg" style="background: url('/images/4pro1Gal4.jpg'); margin-top: 1rem;">
                            </a>
                        </div>
                        <div class="col s8">
                            <a href="/images/4p1Gal5grande.jpg" style="background: url('/images/4pro1Gal5.jpg'); margin-top: 1rem;">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col s3">
                    <a href="/images/4p1Gal6grande.jpg" style="background: url('/images/4pro1Gal6.jpg')">
                    </a>
                </div>
            </div>
        </div>

        <div class="col s12 share">
            <span>Compartir</span>
            <a href="">
                <img src="/images/facebook.svg" alt="facebook">
            </a>
            <a href="">
                <img src="/images/whatsapp.svg" alt="facebook">
            </a>
        </div>


    </div>
@endsection

@section('footer')
<div class="container">
    <div class="row">
        <form action="#" class="col s8 form-contact">
            <p>¿Te interesa conocer más sobre nuestros proyectos?</p>
            <p>Haz tu cita y comienza una nueva vida.</p>
            <div class="spacer"></div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="name" type="text" class="validate">
                    <label for="name">Nombre</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input id="email" type="email" class="validate">
                    <label for="email">Correo Electrónico</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <input id="phone" type="text" class="validate">
                    <label for="phone">Teléfono</label>
                </div>
                <div class="input-field col s6">
                    <input id="city" type="text" class="validate">
                    <label for="city">Ciudad</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <textarea id="message" class="materialize-textarea"></textarea>
                    <label for="message">Mensaje</label>
                </div>
            </div>
            
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn btn-urbano">ENVIAR</button>
                </div>
            </div>
        </form>
        <div class="col s4">
            <h2 class="title">Contacto</h2>
        </div>
    </div>
</div>
@endsection