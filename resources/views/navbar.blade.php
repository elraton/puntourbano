<div class="container">
    <div class="row">
        <div class="col s2 logo">
            <a href="{{ url('/') }}">
                <img src="/images/logo.svg" alt="logo">
            </a>
        </div>
        <div class="col s10 navigation">
            <button class="menu"><i class="material-icons">menu</i></button>
            <ul class="menu_list">
                <li>
                    <a @if (url()->current() == url('about')) class="active" @endif href="{{ url('about') }}">Quienes somos</a>
                </li>
                <li>
                    <a @if (url()->current() == url('projects')) class="active" @endif href="{{ url('projects') }}">Proyectos</a>
                </li>
                <li>
                    <a @if (url()->current() == url('business')) class="active" @endif href="{{ url('business') }}">Lineas de negocio</a>
                </li>
                <li>
                    <a @if (url()->current() == url('oportunities')) class="active" @endif href="{{ url('oportunities') }}">Oportunidades</a>
                </li>
                <li>
                    <a @if (url()->current() == url('news')) class="active" @endif href="{{ url('news') }}">Noticias</a>
                </li>
                <li>
                    <a @if (url()->current() == url('contact')) class="active" @endif href="{{ url('contact') }}">Contacto</a>
                </li>
            </ul>
        </div>
    </div>
</div>